﻿#include <iostream>
#include <cmath>

class Vector {
private:
    double x, y, z;

public:
    Vector(double xVal, double yVal, double zVal) : x(xVal), y(yVal), z(zVal) {}

    void showValues() {
        std::cout << "X: " << x << ", Y: " << y << ", Z: " << z << std::endl;
    }

    double getLength() {
        return sqrt(x * x + y * y + z * z);
    }
};

int main() {
    Vector vec(1.0, 2.0, 3.0);
    vec.showValues();
    std::cout << "Length of the vector: " << vec.getLength() << std::endl;
    return 0;
}
